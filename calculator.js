var tok1, tok2, tok3, tok4, tok5;
var tok10, tok20, tok30, tok40, tok50;
var ai1, ai2, u, bi1, bi2, bu;
var a, v, x; // a - matrix to solve, v - vector of vals, x - result vector
var aim;
var ntok;
var at;
var bt;
var tfr;
var umax;
var a00, tim, dtim, tk, utk;
var mpar;
var tau1;
var tau2;
var coef;
var dl;
var alw;
var dlo;
var alw1, dlp, alw2;
var rn;
var rp;
var ncab;
var dl1, dl2;
var rs1, rs2;
var h1, h2;
var rk1, rk2;
var ro;
var tm;
var dt;
var dd;
var ttt;
var tpp;
var al1, g1, al2, g2;
var constTimeTf, constTimeMomentum;
var n1, n2, nn, nn2;
var aa1, bb1, cc1, dd1, ff1;
var t, tt, tp, ti, tdet;
var mt;
var aa2, bb2, cc2, dd2, ff2;
var ai, n11, n0, n21;
var uu;


/**
 * Starting calculations of ground resistance.
 * @param data
 */
function runCalculator(data) {
    init(data);
    return calculation();
}

/**
 * Calculation.
 */
function calculation() {
    switch (ntok) {
        case '10350': {
            setCoef10350();
            break;
        }
        case '025100': {
            setCoef25100_bi();
            break;
        }
        case 'bi': {
            calcBi();
            break;
        }
        default: {
            let msg = 'Undefined calculation type.';
            alert(msg);
            throw Error(msg);
        }
    }


    //console.log(commonCalc());
    return commonCalc();
}

/**
 * Calculation for common values of all the calculators.
 */
function commonCalc() {
    alw1 = alw * dlo / dl;
    dlp = dl - dlo;
    alw2 = alw - alw1;

    al1 = 0.2 * (Math.log(2 * dl1 / rs1) - 1);
    if (h1 > rs1) {
        g1 = 6.283185 / ro / Math.log(dl1 * dl1 / 2 / rs1 / h1);
    } else {
        g1 = 3.141593 / ro / Math.log(dl1 / rs1);
    }

    if (ncab === 2) {
        al2 = 0.2 * (Math.log(2 * dl2 / rs2) - 1);
        if (h2 > rs2) {
            g2 = 6.283185 / ro / Math.log(dl2 * dl2 / 2 / rs2 / h2);
        } else {
            g2 = 3.141593 / ro / Math.log(dl2 / rs2);
        }
    }

    ttt = ttt - 0.1 * dt;
    tpp = tpp - 0.1 * dt;

    n1 = (dd / 10 + dl1) / dd;
    n2 = (dd / 10 + dl2) / dd;

    if (n2 < 1) {
        n2 = 1;
    }

    // Current calculation of the underground cable 1
    nn = Math.floor(n1 + 1);
    ai1 = Array(nn);
    ai2 = Array(nn);
    u = Array(nn);
    ai1.fill(0);
    ai2.fill(0);
    u.fill(0);

    aa1 = al1 * dd / dt + rk1 + 1 / g1 / dd;
    bb1 = 1 / dd / dd;
    cc1 = al1 * dd / dt;
    dd1 = dt / g1 / al1 / dd / dd;
    ff1 = al1 * dd / dt + rn + 1 / g1 / dd;


    // Current calculation of the underground cable 2
    if (ncab === 2) {
        nn2 = Math.floor(n2 + 1);
        bi1 = Array(nn2);
        bi2 = Array(nn2);
        bu = Array(nn2);
        bi1.fill(0);
        bi2.fill(0);
        bu.fill(0);

        aa2 = al2 * dd / dt + rk2 + 1 / g2 / dd;
        bb2 = 1 / dd / dd;
        cc2 = al2 * dd / dt;
        dd2 = dt / g2 / al2 / dd / dd;
        ff2 = al2 * dd / dt + rn + 1 / g2 / dd;
    }

    t = tt = tp = ti = tdet = 0;
    mt = (tm + dt / 10) / dt;


    // TODO: find go to 100
    let results = [];
    for (let i = 1; i <= mt; i++) {
        results.push(timeCircle());
    }

    return results;
}

/**
 *
 */
function timeCircle() {
    t = t + dt;
    tt = tt + dt;
    tp = tp + dt;
    ti = ti + dt;
    tdet = tdet + dt;

    if (ntok !== CommonCalcData.TYPES.bi) {
        ai = f1(t);
    } else {
        ai = a00 * (Math.exp(-bt * t) - Math.exp(-at * t));
    }

    // TODO: check loop transformation
    // Распределение тока по подземному кабелю № 1
    n11 = n1 - 1;
    for (let i = 1; i < n11; i++) {
        ai1[i] = (ai2[i + 1] - 2 * ai2[i] + ai2[i - 1]) * dd1 + ai2[i];
    }
    // Определение режима на конце подземного кабеля № 1
    ai1[n1 - 1] = (ai1[n1 - 2] / g1 / dd + ai2[n1 - 1] * cc1) / aa1;

    // Определение напряжения в расчетных узлах
    u[n1] = ai1[n1 - 1] * rk1;
    n0 = n1 - 2;
    for (let i = n1 - 1; i > 1; i--) {
        u[i] = u[i + 1] + (ai1[i] - ai2[i]) * cc1;
    }

    if (ncab === 2) {
        // Распределение тока по подземному кабелю № 2
        n21 = n2 - 1;
        for (let i = 1; i < n21; i++) {
            bi1[i] = (bi2[i + 1] - 2 * bi2[i] + bi2[i - 1]) * dd2 + bi2[i];
        }

        // Определение режима на конце подземного кабеля № 2
        bi1[n2 - 1] = (bi1[n2 - 2] / g2 / dd + bi2[n2 - 1] * cc2) / aa2;

        // Определение напряжения в расчетных узлах кабеля № 2
        bu[n2] = bi1[n2 - 1] * rk2;
        n0 = n2 - 2;
        for (let i = n2 - 1; i > 1; i--) {
            bu[i] = bu[i + 1] + (bi1[i] - bi2[i]) * cc2;
        }
    }

    // Определение тока первого узла и режима в заземлителе объекта
    // Формирование матрицы для расчета режима
    // Столбец свободных членов, вектор - последний столбец матрицы,
    // состоящий из последних значений каждой строки.

    if (ncab === 2) {
        a[0][0] = 1;
        a[0][1] = 1;
        a[0][2] = 0;
        a[0][3] = 0;
        a[0][4] = 0;
        a[1][0] = 1;
        a[1][1] = 0;
        a[1][2] = -1;
        a[1][3] = -1;
        a[1][4] = -1;
        a[2][0] = -alw1 / dt;
        a[2][1] = alw2 / dt + rp;
        a[2][2] = -rn;
        a[2][3] = 0;
        a[2][4] = 0;
        a[3][0] = 0;
        a[3][1] = 0;
        a[3][2] = -rn;
        a[3][3] = cc1;
        a[3][4] = 0;
        a[4][0] = 0;
        a[4][1] = 0;
        a[4][2] = -rn;
        a[4][3] = 0;
        a[4][4] = cc2;
        v[0] = ai;
        v[1] = 0;
        v[2] = -alw1 / dt * tok10 + alw2 / dt * tok20;
        v[3] = cc1 * tok40 - u[1];
        v[4] = cc2 * tok50 - bu[1];
    } else {
        a[0][0] = 1;
        a[0][1] = 1;
        a[0][2] = 0;
        a[0][3] = 0;
        a[1][0] = 1;
        a[1][1] = 0;
        a[1][2] = -1;
        a[1][3] = -1;
        a[2][0] = -alw1 / dt;
        a[2][1] = alw2 / dt + rp;
        a[2][2] = -rn;
        a[2][3] = 0;
        a[3][0] = 0;
        a[3][1] = 0;
        a[3][2] = -rn;
        a[3][3] = cc1;
        v[0] = ai;
        v[1] = 0;
        v[2] = -alw1 / dt * tok10 + alw2 / dt * tok20;
        v[3] = cc1 * tok40 - u[1];
    }

    x = gauss(a, v);

    if (ncab === 2) {
        tok1 = x[0];
        tok2 = x[1];
        tok3 = x[2];
        tok4 = x[3];
        tok5 = x[4];
    } else {
        tok1 = x[0];
        tok2 = x[1];
        tok3 = x[2];
        tok4 = x[3];
    }

    ai1[0] = tok4;
    u[0] = rn * tok3;

    if (ncab === 2) {
        bi1[0] = tok5;
        bu[0] = u[0];
    }

    // Запись режима очередного расчетного шага
    nn = n1 + 1;
    for (let i = 0; i < nn; i++) {
        uu = ai2[i];
        ai2[i] = ai1[i];
        ai1[i] = uu;
    }

    if (ncab === 2) {
        nn = n2 + 1;
        for (let i = 0; i < nn; i++) {
            uu = bi2[i];
            bi2[i] = bi1[i];
            bi1[i] = uu;
        }
    }

    // Вывод расчетных данных
    if (tt >= ttt) {
        tt = 0;
    }

    if (tp >= tpp) {
        tp = 0;
    }

    tok10 = tok1;
    tok20 = tok2;
    tok30 = tok3;
    tok40 = tok4;
    tok50 = tok5;

    let finObj = {};

    finObj.t = t.toExponential();
    finObj.Im = ai.toExponential();
    finObj.Ivl1 = tok1.toExponential();
    finObj.Ivl2 = tok2.toExponential();
    finObj.Iground = tok3.toExponential();
    finObj.Ikab1 = tok4.toExponential();

    if (ncab === 2) {
        finObj.Ikab2 = tok5.toExponential();
    }

    return finObj;
}

/**
 * Calculations specific to biexp momentum of current.
 */
function calcBi() {
    constTimeTf = 1 / at;
    constTimeMomentum = 1 / bt;

    tfr = Math.log(at / bt) / (at - bt);
    umax = Math.exp(-bt * tfr) - Math.exp(-at * tfr);
    a00 = aim / umax;
    tim = constTimeMomentum;
    dtim = tim / 100;
    tim = 5 * tim;

    for (let i = 0; k < 500; i++) {
        tk = tfr + i * dtim;
        utk = Math.exp(-bt * tk) - Math.exp(-at * tk);

        let temp = umax / 2;
        if (utk <= temp) {
            break;
        }
    }

    tim = tk;

    setCoef25100_bi();
}


function setCoef10350() {
    tau1 = 1.9;
    tau2 = 4.85;
    coef = 0.93;
}

function setCoef25100_bi() {
    tau1 = 0.454;
    tau2 = 1.43;
    coef = 0.93;
}

/**
 * Initialization for all calculator variables.
 * @param allData The object with values gathered from calc forms.
 */
function init(allData) {
    ntok = allData.calcType;
    ncab = allData.cablesCount;

    aim = Number(allData.current_amplitude);
    dl = Number(allData.len_vl_from_vs);
    alw = Number(allData.full_ind);
    dlo = Number(allData.point_light_attack);
    rn = Number(allData.res_ground_obj);
    rp = Number(allData.res_ground_vl_ps);
    ro = Number(allData.specific_ground_res);
    tm = Number(allData.time_calc_tm);
    dt = Number(allData.time_step_dt);
    dd = Number(allData.channel_length_step);
    ttt = Number(allData.display_step_ttt);
    tpp = Number(allData.print_step_tpp);
    dl1 = Number(allData.cable_len_1);
    rs1 = Number(allData.cable_rad_1);
    h1 = Number(allData.cable_depth_1);
    rk1 = Number(allData.cable_tail_res_1);

    if (ncab === 2) {
        dl2 = allData.cable_len_2;
        rs2 = allData.cable_rad_2;
        h2 = allData.cable_depth_2;
        rk2 = allData.cable_tail_res_2;
    }

    if (ntok === TYPES_OF_CALC.bi) {
        at = allData.bi_const_front;
        bt = allData.bi_const_tail;
    }

    // ai1 = Array(2001);
    // ai2 = Array(2001);
    // bi1 = Array(2001);
    // bi2 = Array(2001);
    // u = Array(2001);
    // bu = Array(2001);
    if (ncab === 2) {
        a = Array(Array(5), Array(5), Array(5), Array(5), Array(5));
        v = Array(5);
    } else {
        a = Array(Array(4), Array(4), Array(4), Array(4));
        v = Array(4);
    }

    tok1 = tok2 = tok3 = tok4 = tok5 = tok10 = tok20 = tok30 = tok40 = tok50 = 0;
}

/**
 * The formula of calculation by T.
 * @param t
 * @return {number}
 */
function f1(t) {
    let ttau1Pow = Math.pow(t / tau1, 10);
    let ttau2Exp = Math.exp(-t / tau2);
    return aim * ttau1Pow * ttau2Exp / coef / (1 + ttau1Pow);
}