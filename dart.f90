program dart_uzip
    c       Включение источника тока I(t) на подземные кабели с утечкой
    c       длиной DL1 и DL2, gL- приближение. Конец линии замкнут на резистор RK
    c       начало - на 'эквивалентную схему, имитирующую ВЛ и сосредоточенный
    c       заземлитель

    c    tok1 - ток в ВЛ к объекту
    c       tok2 - ток в ВЛ к ПС
    c       tok3 -ток в заземлителе объекта
    c       tok4 - ток в начале подземного кабеля № 1
    c       tok5 - ток в начале подземного кабеля № 3
    c       tok*0 - ток в предшествующий момент времени

    implicit real*8 (a-h, o-y)
    common /U/ A, ncab
    DIMENSION AI1(2001), AI2(2001), U(2001)
    DIMENSION BI1(2001), BI2(2001), BU(2001), A(6, 6)

    c f1(t) in JS
    f1(t) = aim * (t / tau1)**10 * dexp(-t / tau2) / coef / (1.d0 + (t / tau1)**10)

    CALL system('chcp 1251')

    PRINT*, 'Амплитуда тока - ? (кА)'
    read*, AIM
    print*, 'Импульс тока  10/350 -(1)    0,25/100  - (2)'
    print*, 'Биэкспоненциальный импульс - (3)'
    read*, ntok

    c calcBi() in JS
    end program dart_uzip
    if(ntok.eq.3) then
        67    print*, 'Константы фронта (а) и хвоста (b) импульса (1/мкс)'
        read*, at, bt
        tfr = dlog(at / bt) / (at - bt)
        umax = dexp(-bt * tfr) - dexp(-at * tfr)
        a00 = aim / umax  !!!!
        tim = 1. / bt
        dtim = tim / 100
        tim = 5. * tim
        do k = 1, 500
            tk = tfr + k * dtim
            utk = dexp(-bt * tk) - dexp(-at * tk)
            if(utk.le.umax / 2.) goto 66
        end do
        66    tim = tk
        zr1 = tfr
        print*, 'Время максимума -', zr1, ' мкс'
        zr1 = tim
        print*, 'Длительность импульса -', zr1, ' мкс'
        print*, 'Скорректировать параметры -? ДА -(1), НЕТ -(0)'
        read*, mpar
        if(mpar.eq.1) go to 67

    end if

    if(ntok.lt.2) then

        tau2 = 4.85d2
        tau1 = 1.9d1
        coef = 0.93d0
    else
        tau2 = 1.43d2
        tau1 = 0.454d0
        coef = 0.93d0
    end if

    print*, 'Длина ВЛ от объекта до ПС-? (м)'
    read*, DL
    print*, 'Полная индуктивность ВЛ -? (мкГн)'
    read*, alW
    print*, 'Удаление точки от объекта точки удара молнии-(м)'
    read*, dlo

    c       Оценка участков ВЛ и и х индуктивностей
    alw1 = alw * dlo / dl
    dlp = dl - dlo
    alw2 = alw - alw1
    print*, 'Сопротивление заземление объекта -? (Ом)'
    read*, RN
    print*, 'Сопротивление заземления ВЛ на ПС -? (Ом)'
    read*, RP
    print*, 'Число подземных кабелей 1 или 2 ?'
    read*, ncab
    PRINT*, 'Длина подземного кабеля № 1 -?  (м)'
    READ*, DL1
    print*, 'Радиус кабеля № 1- ? (м)'
    read*, rs1
    print*, 'Глубина укладки кабеля № 1- ? (м)'
    read*, h1
    PRINT*, 'Cопротивление в конце кабеля № 1 - ?  (Ом)'
    READ*, RK1

    if(ncab.eq.2) then
        PRINT*, 'Длина подземного кабеля № 2 -?  (м)'
        READ*, DL2
        print*, 'Радиус кабеля - № 2 ? (м)'
        read*, rs2
        print*, 'Глубина укладки кабеля № 2- ? (м)'
        read*, h2
        PRINT*, 'Cопротивление в конце кабеля № 2 - ?  (Ом)'
        READ*, RK2
    end if

    print*, 'Удельное сопротивление грунта - ? (Ом м)'
    read*, ro
    PRINT*, 'Расчетное время TM, временной шаг DT  (мкс)'
    READ*, TM, DT
    print*, 'Шаг по длине канала (м)'
    read*, dd
    PRINT*, 'Шаг вывода на дисплей TTT, на печать (мкс)'
    READ*, TTT, TPP
    c            Параметры подземных кабелей
    al1 = 0.2 * (dlog(2.d0 * dl1 / rs1) - 1.d0)
    if(h1.gt.rs1) then
        g1 = 6.283185 / ro / dlog(dl1 * dl1 / 2.d0 / rs1 / h1)
    else
        g1 = 3.141593 / ro / dlog(dl1 / rs1)
    end if

    IF(NCAB.EQ.2) THEN
        al2 = 0.2 * (dlog(2.d0 * dl2 / rs2) - 1.d0)
        if(h2.gt.rs2) then
            g2 = 6.283185 / ro / dlog(dl2 * dl2 / 2.d0 / rs2 / h2)
        else
            g2 = 3.141593 / ro / dlog(dl2 / rs2)
        end if
    END IF

    ZR1 = AIM
    write(14, *) '  '
    write(14, *) '                                  .  .2018'
    WRITE(14, *) 'Распределение тока молнии между подз. коммуникацией,'
    WRITE(14, *) '                ВЛ и заземлителем объекта '
    WRITE(14, *) '                              Программа dart_uzip'
    write(14, *) '         (gL-приближение LR- приближение ВЛ)'
    WRITE(14, *) '  '
    WRITE(14, *) ' Амплитуда тока =', ZR1, ' кА'

    if(ntok.eq.1) then
        write(14, *) ' Импульс тока молнии 10/350 мкс'
    end if
    if(ntok.eq.2) THEN
        write(14, *) ' Импульс тока молнии 0,25/100 мкс'
    end if
    IF(NTOK.EQ.3) THEN
        write(14, *) ' Биэкспоненциальный импульс тока'
        zr1 = 1. / at
        zr2 = 1. / bt
        write(14, *) ' Постоянные времени Тф, Тимп = ', zr1, zr2, ' мкс'
    end if

    zr1 = dl
    write(14, *) ' Длина ВЛ - ', zr1, ' м'
    zr1 = alw
    write(14, *) ' Полная индуктивность ВЛ - ', zr1, ' мкГн'
    zr1 = dlo
    write(14, *) ' Удаление точки удара от объекта - ', zr1, ' м'
    zr1 = rn
    write(14, *) ' Сопротивление заземления объекта - ', zr1, ' Ом'
    zr1 = rp
    write(14, *) ' Сопротивление заземления ПС - ', zr1, ' Ом'

    ZR1 = DL1
    WRITE(14, *) ' Длина подземного кабеля № 1- ', ZR1, ' м'
    zr1 = rs1
    write(14, *) ' Радиус подземного кабеля № 1- ', zr1, ' м'
    zr1 = h1
    write(14, *) ' Глубина подземного кабеля № 1- ', zr1, ' м'
    zr1 = al1
    write(14, *) ' Погонная индуктивность кабеля №1- ', zr1, ' МкГн/м'
    zr2 = g1
    write(14, *) ' Погонная проводимость кабеля №1- ', zr2, ' 1/Ом м'
    ZR1 = RK1
    WRITE(14, *) ' Сопротивление в конце кабеля № 1- ', zr1, ' Ом'

    if(ncab.eq.2) Then
        ZR1 = DL2
        WRITE(14, *) ' Длина подземного кабеля № 2- ', ZR1, ' м'
        zr1 = rs2
        write(14, *) ' Радиус подземного кабеля № 2- ', zr1, ' м'
        zr1 = h2
        write(14, *) ' Глубина подземного кабеля № 2- ', zr1, ' м'
        zr1 = al2
        write(14, *) ' Погонная индуктивность кабеля №2- ', zr1, ' МкГн/м'
        zr2 = g2
        write(14, *) ' Погонная проводимость кабеля №2- ', zr2, ' 1/Ом м'
        ZR1 = RK2
        WRITE(14, *) ' Сопротивление в конце кабеля № 2- ', zr1, ' Ом'
    end if

    zr1 = ro
    write(14, *) ' Удельное сопротивление грунта - ', zr1, ' Ом м '
    ZR1 = TM
    ZR2 = DT
    WRITE(14, *) 'Время расчета, шаг расчета', ZR1, ZR2, ' мкс'
    zr1 = dd
    write(14, *) 'Пространственный шаг расчета = ', zr1, ' м'
    WRITE(14, *) '           ----------'
    write(14, *) '  '

    if(ncab.eq.2) then
        write(14, 300)
        write(14, 301)
        write(14, *) '  '
    else
        write(14, 320)
        write(14, 321)
        write(14, *) '  '
    end if

    ZR1 = AIM
    write(12, *) '  '
    write(12, *) '                                  .  .2018'
    WRITE(12, *) 'Распределение тока молнии между подз. коммуникацией,'
    WRITE(12, *) '                ВЛ и заземлителем объекта '
    WRITE(12, *) '                              Программа dart_uzip'
    write(12, *) '         (gL-приближение LR- приближение ВЛ)'
    WRITE(12, *) '  '
    WRITE(12, *) ' Амплитуда тока =', ZR1, ' кА'

    if(ntok.eq.1) then
        write(12, *) ' Импульс тока молнии 10/350 мкс'
    end if
    if(ntok.eq.2) THEN
        write(12, *) ' Импульс тока молнии 0,25/100 мкс'
    end if
    IF(NTOK.EQ.3) THEN
        write(12, *) ' Биэкспоненциальный импульс тока'
        zr1 = 1. / at
        zr2 = 1. / bt
        write(12, *) ' Постоянные времени Тф, Тимп = ', zr1, zr2, ' мкс'
    end if

    zr1 = dl
    write(12, *) ' Длина ВЛ - ', zr1, ' м'
    zr1 = alw
    write(12, *) ' Полная индуктивность ВЛ - ', zr1, ' мкГн'
    zr1 = dlo
    write(12, *) ' Удаление точки удара от объекта - ', zr1, ' м'
    zr1 = rn
    write(12, *) ' Сопротивление заземления объекта - ', zr1, ' Ом'
    zr1 = rp
    write(12, *) ' Сопротивление заземления ПС - ', zr1, ' Ом'

    ZR1 = DL1
    WRITE(12, *) ' Длина подземного кабеля № 1- ', ZR1, ' м'
    zr1 = rs1
    write(12, *) ' Радиус подземного кабеля № 1- ', zr1, ' м'
    zr1 = h1
    write(12, *) ' Глубина подземного кабеля № 1- ', zr1, ' м'
    zr1 = al1
    write(12, *) ' Погонная индуктивность кабеля №1- ', zr1, ' МкГн/м'
    zr2 = g1
    write(12, *) ' Погонная проводимость кабеля №1- ', zr1, ' 1/Ом м'
    ZR1 = RK1
    WRITE(12, *) ' Сопротивление в конце кабеля № 1- ', zr1, ' Ом'

    if(ncab.eq.2) then
        ZR1 = DL2
        WRITE(12, *) ' Длина подземного кабеля № 2- ', ZR1, ' м'
        zr1 = rs2
        write(12, *) ' Радиус подземного кабеля № 2- ', zr1, ' м'
        zr1 = h2
        write(12, *) ' Глубина подземного кабеля № 2- ', zr1, ' м'
        zr1 = al2
        write(12, *) ' Погонная индуктивность кабеля №2- ', zr1, ' МкГн/м'
        zr2 = g2
        write(12, *) ' Погонная проводимость кабеля №2- ', zr1, ' 1/Ом м'
        ZR1 = RK2
        WRITE(12, *) ' Сопротивление в конце кабеля № 2- ', zr1, ' Ом'
    end if

    zr1 = ro
    write(12, *) ' Удельное сопротивление грунта - ', zr1, ' Ом м '
    ZR1 = TM
    ZR2 = DT
    WRITE(12, *) 'Время расчета, шаг расчета', ZR1, ZR2, ' мкс'
    zr1 = dd
    write(12, *) 'Пространственный шаг расчета = ', zr1, ' м'
    WRITE(12, *) '           ----------'
    write(12, *) '  '
    write(12, 310)
    write(12, 311)
    write(12, *) '  '

    TTT = TTT - 0.1 * DT
    TPP = TPP - 0.1 * DT

    N1 = (DD / 10. + DL1) / DD
    N2 = (dd / 10. + DL2) / dd
    if(n2.lt.1) n2 = 1

    c            Задание начальных условий
    c   1  CONTINUE
    c          Начальные условия для подземного кабеля № 1
    NN = N1 + 1

    DO K = 1, NN
        AI1(K) = 0.
        AI2(K) = 0.
        U(K) = 0.
    end do

    c         Начальные условия для подземного кабеля  № 2
    if(ncab.eq.2) then
        nn2 = n2 + 1

        DO K = 1, NN2
            BI1(K) = 0.
            BI2(K) = 0.
            BU(K) = 0.
        end do
    end if

    tok1 = 0.
    tok2 = 0.
    tok3 = 0.
    tok4 = 0.
    tok5 = 0.
    tok10 = 0.
    tok20 = 0.
    tok30 = 0.
    tok40 = 0.
    tok50 = 0.

    c         Константы для расчета тока в подземном кабеле № 1
    AA1 = AL1 * DD / DT + RK1 + 1. / G1 / DD
    BB1 = 1. / DD / DD
    CC1 = AL1 * DD / DT
    DD1 = DT / G1 / AL1 / DD / DD
    FF1 = AL1 * DD / DT + RN + 1. / G1 / DD
    T = 0.
    TT = 0.
    TP = 0.
    TI = 0.
    tdet = 0.
    MT = (TM + DT / 10.) / DT

    c         Константы для расчета тока в подземном кабеле № 2

    if(ncab.eq.2) then
        AA2 = AL2 * DD / DT + RK2 + 1. / G2 / DD
        BB2 = 1. / DD / DD
        CC2 = AL2 * DD / DT
        DD2 = DT / G2 / AL2 / DD / DD
        FF2 = AL2 * DD / DT + RN + 1. / G2 / DD
    end if


    c            Цикл по времени
    c  100  CONTINUE
    do l = 1, mt
        T = T + DT
        TT = TT + DT
        TP = TP + DT
        TI = TI + DT
        tdet = tdet + dt

        if(ntok.lt.3) then
            AI = F1(T)
        else
            ai = a00 * (dexp(-bt * t) - dexp(-at * t))
        end if

        c           Распределение тока по подземному кабелю № 1
        N11 = N1 - 1
        DO K = 2, N11
            AI1(K) = (AI2(K + 1) - 2. * AI2(K) + AI2(K - 1)) * DD1 + AI2(K)
        END DO

        c        Определение режима на конце подземного кабеля № 1
        AI1(N1) = (AI1(N1 - 1) / G1 / DD + AI2(N1) * CC1) / AA1

        c          Определение напряжения в расчетных узлах
        U(N1 + 1) = AI1(N1) * RK1
        N0 = N1 - 2
        DO K = N1, 2, -1
            U(K) = U(K + 1) + (AI1(K) - AI2(K)) * CC1
        END DO

        if(ncab.eq.2) then
            c           Распределение тока по подземному кабелю № 2
            N21 = N2 - 1
            DO K = 2, N21
                BI1(K) = (BI2(K + 1) - 2. * BI2(K) + BI2(K - 1)) * DD2 + BI2(K)
            END DO

            c        Определение режима на конце подземного кабеля № 2
            BI1(N2) = (BI1(N2 - 1) / G2 / DD + BI2(N2) * CC2) / AA2

            c          Определение напряжения в расчетных узлах кабеля № 2
            BU(N2 + 1) = BI1(N2) * RK2
            N0 = N2 - 2
            DO K = N2, 2, -1
                BU(K) = BU(K + 1) + (BI1(K) - BI2(K)) * CC2
            END DO
        end if

        c           Определение тока первого узла и режима в заземлителе объекта

        c               Формирование матрицы для расчета режима

        if(ncab.eq.2) then

            a(1, 1) = 1.
            a(1, 2) = 1.
            a(1, 3) = 0.
            a(1, 4) = 0.
            a(1, 5) = 0.
            a(1, 6) = ai
            a(2, 1) = 1.
            a(2, 2) = 0.
            A(2, 3) = -1.
            a(2, 4) = -1.
            a(2, 5) = -1.
            a(2, 6) = 0.
            a(3, 1) = -alw1 / dt
            a(3, 2) = alw2 / dt + rp
            a(3, 3) = -rn
            a(3, 4) = 0.
            a(3, 5) = 0.
            a(3, 6) = -alw1 / dt * tok10 + alw2 / dt * tok20
            a(4, 1) = 0.
            a(4, 2) = 0.
            a(4, 3) = -rn
            a(4, 4) = cc1
            a(4, 5) = 0.
            a(4, 6) = cc1 * tok40 - u(2)
            a(5, 1) = 0.
            a(5, 2) = 0.
            a(5, 3) = -rn
            a(5, 4) = 0.
            a(5, 5) = cc2
            a(5, 6) = cc2 * tok50 - bu(2)

        else

            a(1, 1) = 1.
            a(1, 2) = 1.
            a(1, 3) = 0.
            a(1, 4) = 0.
            a(1, 5) = ai
            a(2, 1) = 1.
            a(2, 2) = 0.
            A(2, 3) = -1.
            a(2, 4) = -1.
            a(2, 5) = 0.
            a(3, 1) = -alw1 / dt
            a(3, 2) = alw2 / dt + rp
            a(3, 3) = -rn
            a(3, 4) = 0.
            a(3, 5) = -alw1 / dt * tok10 + alw2 / dt * tok20
            a(4, 1) = 0.
            a(4, 2) = 0.
            a(4, 3) = -rn
            a(4, 4) = cc1
            a(4, 5) = cc1 * tok40 - u(2)

        end if

        CALL GAUSS

        if(ncab.eq.2) then
            tok1 = a(5, 1)
            tok2 = a(5, 2)
            tok3 = a(5, 3)
            tok4 = a(5, 4)
            tok5 = a(5, 5)

        else
            tok1 = a(4, 1)
            tok2 = a(4, 2)
            tok3 = a(4, 3)
            tok4 = a(4, 4)
        end if

        AI1(1) = TOK4
        U(1) = RN * TOK3

        if(ncab.eq.2) then
            BI1(1) = TOK5
            BU(1) = U(1)
        end if

        c          Запись режима очередного расчетного шага
        NN = N1 + 1
        DO K = 1, NN
            UU = AI2(K)
            AI2(K) = AI1(K)
            AI1(K) = UU
        END DO

        if(ncab.eq.2) then
            NN = N2 + 1
            DO K = 1, NN
                UU = BI2(K)
                BI2(K) = BI1(K)
                BI1(K) = UU
            END DO
        end if


        c            Вывод расчетных данных

        IF(TT.GE.TTT) THEN
            zr1 = t
            zr2 = AI1(N1)
            zr3 = U(N2 + 1)
            print*, 't = ', zr1, ' I = ', zr2, ' кА   U = ', zr3
            tt = 0
        end if

        300   FORMAT(1X, '   t мкс   Iм кA    Ikab1 кА  Ikab2 кА   Iзем кА')

        301   format(1x, '--------------------------------------------------')
        302   format(1x, E8.3, 2x, E10.3, 1X, E10.3, 1x, E10.3, 1X, E10.3)

        310   FORMAT(1X, '   t мкс   Iм кA    Iвл1 кА  Iвл2 кА   Iзем кА')

        311   format(1x, '--------------------------------------------------')
        312   format(1x, E8.3, 2x, E10.3, 1X, E10.3, 1x, E10.3, 1X, E10.3)

        320   FORMAT(1X, '   t мкс   Iм кA    Ikab1 кА  Iзем кА')
        321   format(1x, '---------------------------------------------')
        322   format(1x, E8.3, 2x, E10.3, 1X, E10.3, 1x, E10.3)

        IF(TP.GE.TPP) THEN
            TP = 0.
            zr1 = t

            if(ncab.eq.2) then
                write(14, 302)zr1, ai, tok4, tok5, tok3
                write(12, 312)zr1, ai, tok1, tok2, tok3
            else
                write(14, 322)zr1, ai, tok4, tok3
                write(12, 312)zr1, ai, tok1, tok2, tok3
            end if
        end if

        TOK10 = TOK1
        TOK20 = TOK2
        TOK30 = TOK3
        TOK40 = TOK4
        TOK50 = TOK5

        c    IF(TM.GE.T) GO TO 100
    end do

    c    PRINT*, 'Продолжить счет -? НЕТ -(0), ДА- Любое целое'
    c    READ*, NN0
    c    IF(NN0.GT.0) THEN
    c    PRINT*, 'Время конца счета, шаг записи, вывода на дисплей (мкс)'
    c    READ*, TMM, TPP, TTT
    c    TM = TMM-T
    c    MT=(TM+0.1*DT)/DT
    c    TPP = TPP-0.1*DT
    c    GO TO 100
    c    END IF
        STOP
        END



        C   Программый блок решения системы уравнения методом Гауса
        C   с делением на главный член
        C   Коэффициенты набираются по строкам, знак члена в правой
        C   части не меняется
        SUBROUTINE GAUSS
                IMPLICIT REAL*8(A-H, O-Z)
                COMMON /U/ A, ncab

                DIMENSION A(6, 6)
                N = 5
                if(ncab.eq.1) n = 4
        NN = N+1
                c    DO K = 1, N
                c    DO I = 1, NN
                c    PRINT*,'A', K, I
                c    READ*,A(K, I)
                c    END DO
                c    END DO
                c                Начало собственно программы
                N4 = N-1
                N5 = N+1
                DO 999 M =1, N4
                A1 = 0.
                c           Поиск главного члена
        DO I = M, N
                DO K = M, N
                A2 = ABS(A(I, K))
                IF(A1.LE.A2) THEN
                A1 = A2
                I1 = I
                K1 = K
                END IF
                END DO
                END DO
                c          Перестановка строк с делением на главный член
                A2 = A(I1, K1)
        DO K = M, N5
        A1 =A(M, K)
                A(M, K)= A(I1, K)
                A(I1, K)= A1
                A(M, K)= A(M, K)/A2
        END DO
        c            Перестановка столбцов
                DO I = M, N
                A1 = A(I, M)
                A(I, M)= A(I, K1)
                A(I, K1)= A1
        END DO
        c              Построение треугольной матрицы
        A(M, 1)= K1+0.1
        M5 = M+1
                DO 999 I = M5, N
                A1= A(I, M)
                DO 999 K = M, N5
                A(I, K)= A(I, K)-A1*A(M, K)
                999   CONTINUE
                c           Определение корней
                A(N, N)= A(N, N5)/A(N, N)
                DO J = 1, N4
                J1 =N-J
                X1 = A(J1, N5)
        DO L = 1, J
        L1 =N-L+1
        X1 = X1-A(N, L1)*A(J1, L1)
                END DO
                A(N, J1)= X1
        K1 = A(J1, 1)
        A2 = A(N, K1)
                A(N, K1)= X1
                A(N, J1)= A2
                END DO
                c           Конец решения системы уравнений
                c           Корни уравнений записаны в строке N
                c    DO I = 1, N
                c    PRINT*, A(N, I)
!    END DO
RETURN
END